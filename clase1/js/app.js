console.log( 'TOOLBOX - SEMANA 1' );

const miFuncion = () => {
    console.log( 'hola mundo' );
    return 0;
};


// definicion de variables por cada tipo
var unaFuncion = new Function( "holaMundo" );
var unNumero = 1;
var unaPalabra = "Hola mundo";
var unaFecha = new Date();
var unDato;

console.log( typeof miFuncion === typeof unaFuncion );


// muestra en consola  tipos de datos
console.log( unaFuncion, typeof unaFuncion );
console.log( unNumero, typeof unNumero );
console.log( unaPalabra, typeof unaPalabra );
console.log( unaFecha, typeof unaFecha );
console.log( unDato, typeof unDato );


typeof undefined === "undefined"; // true
typeof true === "boolean"; // true
typeof 42 === "number"; // true
typeof "42" === "string"; // true
typeof { life: 42 } === "object"; // true

// Añadido en ES6!
typeof Symbol() === "symbol"; // true
// Symbols
let symbol1 = Symbol( 'a' );
let symbol2 = Symbol( 'a' );
console.log( typeof symbol1 );
console.log( { symbol2 } );

console.log( symbol1 === symbol2 );
