const users = [
    {
        username: "mauricio",
        password: "sierra",
        isAdmin: true
    },
    {
        username: "admin",
        password: "secret",
        isAdmin: false
    }
];

const one_user = {
    username: 'admin',
    nombre: 'administrador',
    apellido: 'superuser',
    email: 'admin@gmail.com',
    telefono: '44444',
    direccion: 'calle admin',
    password: 'admin123',
    isAdmin: true

};

const findAll = () => {
    return users;
};

const createNewUser = ( user ) => {

    console.log( user );

    const { username, nombre, apellido, email, telefono, direccion, password } = user;

    // valida todos los campos esten diligenciados
    if (
        username !== '' &&
        nombre !== '' &&
        apellido !== '' &&
        email !== '' &&
        telefono !== '' &&
        direccion !== '' &&
        password !== ''

    ) {
        // valida si no existe el usuario
        const existe = users.filter( user => user.email === email );

        // console.log( {existe} );

        if ( existe.length > 0 ) {

            // valida la contraseña
            if ( password.length > 6 ) {

                // se define que no es administrador por defecto
                user.isAdmin = false;

                // inserta usuario en array
                users.push( user );


                return {
                    status: true,
                    msg: 'Usuario creado correctamente',
                };


            } else {
                return {
                    status: false,
                    msg: 'la contraseña debe ser mayot a 6 caracteres',
                };

            }

        } {

            return {
                status: false,
                msg: 'ya existe usuario registrado con ese email',
            };

        }

    } else {
        return {
            status: false,
            msg: 'por favor diligenciar todos los campos',
        };
    }





};

const login = ( username, password ) => {
    // valida campos que esten 
    if ( username !== '' && password !== '' ) {

        const res = users.filter( user => user.username === username && user.password === password );

        console.log( res );

        if ( res ) {
            return {
                status: true,
                msg: 'Login exitoso',
            };
        } else {
            return {
                status: true,
                msg: 'error credenciales',
            };
        }

    } else {
        return {
            status: false,
            msg: 'por favor diligenciar todos los campos',
        };
    }

};

module.exports = { findAll, createNewUser, login };