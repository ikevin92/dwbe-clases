const express = require( 'express' );
const router = express.Router();
const User = require( '../models/user.model' );

/**
 * @swagger
 * /users:
 *  get:
 *      summary: Return the list of users
 *      tags: [Users]
 *      responses:
 *          200:
 *              description: The list of users
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/User'
 *          401:
 *              description: username and password are required
 */
router.get( '/', ( req, res ) => {
    console.log( req.auth );
    res.json( User.findAll() );
} );

/**
 * @swagger
 * /users/{username}:
 *  get:
 *      summary: Get specific user by username
 *      tags: [Users]
 *      parameters:
 *          -   in: path
 *              name: username
 *              schema:
 *                  type: string
 *              required: true
 *              description: The user name
 *      responses:
 *          200:
 *              description: The list of users
 *              content: 
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/User'
 *          401:
 *              description: username and password are required
 */
router.get( '/:username', ( req, res ) => {
    const { username } = req.params;
    console.log( req.auth );
    res.json( User.findAll().filter( u => u.username === username )[ 0 ] );
} );

/**
 * @swagger
 * /users:
 *  post:
 *      summary: Create new user
 *      tags: [Users]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: The user was created
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: username and password are required
 */
router.post( '/', ( req, res ) => {
    const {status, msg} =User.createNewUser( req.body );
    res.json( {
        status, msg
    } );
} );

/**
 * @swagger
 * /users/{username}:
 *  put:
 *      summary: Update a new user
 *      tags: [Users]
 *      parameters:
 *          -   in: path
 *              name: username
 *              schema:
 *                  type: string
 *              required: true
 *              description: the user name to update
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: The user was updated
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/User'
 *          401:
 *              description: username and password are requireds
 */
router.put( '/:username', ( req, res ) => {
    const { username } = req.params;
    const user = req.body;
    const index = User.findAll().findIndex( u => u.username === username );
    User.findAll()[ index ].username = user.username;
    User.findAll()[ index ].password = user.password;
    User.findAll()[ index ].isAdmin = user.isAdmin;

    res.json( User.findAll()[ index ] );
} );

router.delete( '/', ( req, res ) => {
    res.json( 'Delete response' );
} );

/**
 * @swagger
 * tags:
 *  name: Users
 *  description: User section
 * 
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - username
 *              - password
 *              - nombre
 *              - apellido
 *              - email
 *              - telefono
 *              - direccion
 *  
 *          properties:
 *              username:
 *                  type: string
 *                  description: the user name
 *              password:
 *                  type: string
 *                  description: the user pass
 *              nombre:
 *                  type: string
 *                  description: the user firstname
 *              apellido:
 *                  type: string
 *                  description: the user lastname
 *              email:
 *                  type: string
 *                  description: the user email
 *              telefono:
 *                  type: string
 *                  description: the user phone
 *              direccion:
 *                  type: string
 *                  description: the user address
 *              isAdmin:
 *                  type: boolean
 *                  description: True if user is admin
 *          example:
 *              username: koel1992
 *              password: "123456"
 *              nombre: kevin
 *              apellido: echeverri
 *              telefono: "344002349"
 *              email: koel@gmail.com
 *              direccion: calle 30
 *              
 */

module.exports = router;