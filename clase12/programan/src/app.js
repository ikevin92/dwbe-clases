const express = require( 'express' );
const app = express();
app.use( express.json() );


const autores = [
    {
        id: 1,
        nombre: 'Kevin',
        apellido: 'Echeverri',
        fechaNacimiento: '24/08/1899',
        libros: [
            {
                id: 1,
                titulo: 'Ficcion',
                descripcion: 'se trata de ficcion',
                anioPublicacion: 1899
            },
            {
                id: 2,
                titulo: 'Biblio',
                descripcion: 'otra recopilacion',
                anioPublicacion: 1963
            }
        ]
    },
    {
        id: 3,
        nombre: 'Orlando',
        apellido: 'Castillo',
        fechaNacimiento: '11/12/1965',
        libros: [
            {
                id: 10,
                titulo: 'Accion',
                descripcion: 'se trata de accion',
                anioPublicacion: 1993
            },
            {
                id: 12,
                titulo: 'Biblio',
                descripcion: 'otra recopilacion',
                anioPublicacion: 1963
            }
        ]
    },

];


// - GET: devuelve todos los autores
app.get( '/autores', ( req, res ) => {

    console.log( autores );

    res.json( autores );

} );

//- POST: crea un nuevo autor
app.post( '/autores', ( req, res ) => {


    const autor = req.body;
    console.log( autor );

    autores.push( autor );

    res.json( 'Autor Creado' );

} );


// - GET: devuelve el autor con el id indicado
app.get( '/autores/:id', ( req, res ) => {

    const { id } = req.params;

    const autor = autores.filter( a => a.id == id );

    if ( autor[ 0 ] ) {
        res.json( autor[ 0 ] );
    } else {
        res.status( 404 ).json(
            {
                status: false,
                msg: "No se encontro autor"
            }
        );
    }

} );

//     - DELETE: elimina el autor con el id indicado
app.delete( '/autores/:id', ( req, res ) => {

    const { id } = req.params;

    // sabemos la posicion del elemento
    const indice = autores.findIndex( a => a.id == id );

    if ( indice >= 0 ) {
        // elimina con indice
        const eliminado = autores.splice( indice, 1 );
        console.log( eliminado );

        res.json(
            {
                status: true,
                msg: 'Autor eliminado'
            }

        );
    } else {
        res.status( 404 ).json(
            {
                status: false,
                msg: "No se encontro autor"
            }
        );
    }


} );


//         - PUT: modifica el autor con el id indicado
app.put( '/autores/:id', ( req, res ) => {


    const { id } = req.params;

    // sabemos la posicion del elemento
    const indice = autores.findIndex( a => a.id == id );

    let autor_modificado = req.body;

    const id_numero = parseInt( id );

    // console.log( { autor_modificado } );

    autores[ indice ] = { id: id_numero, ...autor_modificado };


    console.log( autores );

    res.json( 'Autor Moficiado' );

} );



app.listen( 3000, () => { console.log( 'Escuchando en el puerto 3000' ); } );
