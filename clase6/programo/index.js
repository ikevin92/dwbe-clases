// VAR

// var texto = 'textod ejemplo';

// texto3 = 'esto es el texto 3';


// if ( true ) {
//     var texto2 = 'texto de ejemplo 2';
//     console.log( texto );
// }

// function funcionPruebaVAR () {
//     var texto4 = 'texto4';
// }

// console.log( texto2 );
// console.log( texto3 );
// // console.log( texto4 );

// LET
// let texto1 = 'texto ejemplo';

// texto3 = 'esto es el texto 3';


// if ( true ) {
//     let texto2 = 'texto de ejemplo 2';
//     console.log( texto1 );

//     console.log( texto2 );
//     console.log( texto3 );
// }

// function funcionPruebaVar () {
//     var texto4 = 'texto4';
//     console.log( texto4 );
// }

// funcionPruebaVar()


// const saludar = ( nombre ) => {
//     console.log( `Hola ${ nombre } como estas` );
// };

saludar( 'Kevin' );

function saludar ( nombre ) {
    console.log( 'hola ' + nombre );
}

let saludar2 = function () {
    console.log( 'Hola 2' );
};

const saludar3 = () => {
    console.log( 'Hola 3' );
};


saludar2 = 2;

console.log( saludar );

function suma ( numero1, numero2 ) {
    console.log( numero1 + numero2 );
}

suma( 2, 'texto' );
suma( 2, 3 );
suma( '2', 3 );

const suma2 = ( n1, n2 ) => {
    console.log( n1 + n2 );
};

suma2( 2, 9 );

const suma3 = function ( n1, n2 ) {
    console.log( n1 + n2 );
};

suma3( 12, 9 );

let sumaMas2 = ( n1, n2 ) => console.log( n1 + n2 );

document.getElementById( 'btn' ).addEventListener( 'click', () => {
    sumaMas2( 15, 20 );
    
})