
function mensaje ( nombre = '', sexo ) {

    console.log( '1' );
    console.log( '2' );
    console.log( '3' );
    console.log( '4' );
    let msj = '';

    // validacion del sexo
    if ( sexo[ 0 ] === 'M' ) {
        msj = ( 'bienvenido: ' + nombre );
    } else {
        msj = ( 'bienvenida: ' + nombre );
    }

    return msj; // se retorna la cadena
}


console.log( mensaje( 'Kevin', 'Masculino' ) );
console.log( mensaje( 'Maria', 'Femenino' ) );
console.log( mensaje( 'Orlando', 'Masculino' ) );


/* SCOPE: es lo que le da significado a las variables 
    si es var aplica a todo. Contexto ejecucion

    let y const solo aplica a determinado bloque donde se declarar
*/


// sumar();

// function sumar () {
//     var numero1 = 2;
//     console.log( numero1 + numero2 );
// }

// var numero2 = 4;

sumar();

var numero2 = 4;

function sumar () {
    var numero1 = 2;
    console.log( numero1 + numero2 );
}
