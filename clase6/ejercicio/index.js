const cuadrado = ( numero ) => numero ** 2;
console.log( { cuadrado: cuadrado( 3 ) } );

const factorial = numero => Math.abs( numero ) <= 1 ? 1 : numero * factorial( numero - 1 );
console.log( { factorial: factorial( 7 ) } );

const areaCirculo = ( radio ) => radio * Math.PI;
console.log( { radio: areaCirculo( 15 ) } );

// LOGIN
const credenciales = {
    user: 'admin',
    pass: '12345'
};

const { user, pass } = credenciales;

const login = ( usuario, password ) => usuario === user && password === pass
    ? 'login exitoso'
    : 'error de credenciales';


console.log( login( 'kevin', '1234' ) );
console.log( login( 'admin', '12345' ) );