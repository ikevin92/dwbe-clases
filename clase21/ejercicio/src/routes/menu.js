const { Router } = require( 'express' );
const { getMenus, createMenu, deleteMenu } = require( '../controllers/menu' );



const router = Router();

router.get( '/', async ( req, res ) => {

    const listMenus = await getMenus();

    res.send( listMenus );

} );

router.post( '/', async ( req, res ) => {

    console.log( req.body );
    const { plato, precio, tipo_plato } = req.body;


    const newMenu = await createMenu( plato, precio, tipo_plato );

    res.send( newMenu );
    // res.send( 'post' );

} );

router.delete( '/:id', async ( req, res ) => {

    const { id } = req.params;

    await deleteMenu( id );

    res.send( { msg: 'ok' } );
    // res.send( 'post' );

} );



module.exports = router;