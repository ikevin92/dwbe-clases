const mongoose = require( 'mongoose' );
require( 'dotenv' ).config();
/**
 * Connection strings
 * mongodb://<username>:<password>@<hostname>:<port>/<database_name>
 * mongodb://<hostname>:<port>/<database_name>
 */
const hostname = process.env.HOSTNAME || "localhost";
const port = process.env.PORT || 27017;
const databaseName = process.env.DATABASE || "restaurant";

const connectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

// mongoose.connect(
//     `mongodb://${ hostname }:${ port }/${ databaseName }`,
//     connectionOptions
// );

const url = `mongodb://${ hostname }:${ port }/${ databaseName }`;

mongoose.connect( url, connectionOptions ).
    then( ( db ) => {

        console.log( 'Connected to database ' );
    } )
    .catch( ( err ) => {
        console.error( `Error connecting to the database. \n${ err }` );
    } );

module.exports = mongoose;