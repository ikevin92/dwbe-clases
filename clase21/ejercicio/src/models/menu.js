const mongoose = require( '../db/connection' );
const Schema = mongoose.Schema;

const menuSchema = new Schema( {
    plato: {
        type: String
    },
    precio: {
        type: Number
    },
    tipo_plato: {
        type: String
    }
} );

const Menu = mongoose.model( 'Menu', menuSchema );

module.exports = Menu;

