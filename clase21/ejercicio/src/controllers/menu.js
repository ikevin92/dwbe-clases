const Menu = require( '../models/menu' );



const createMenu = async ( plato, precio, tipo_plato ) => {

    const menu = new Menu();

    menu.plato = plato;
    menu.precio = precio;
    menu.tipo_plato = tipo_plato;

    await menu.save();
};

const getMenus = async () => {

    // const menu = new Menu();
    const listMenus = await Menu.find();

    return listMenus;

};

const deleteMenu = async ( id ) => {

    await Menu.deleteOne( { _id: id } );

};

module.exports = {
    createMenu,
    getMenus,
    deleteMenu
};