// const { createMenu, getMenus } = require( './controllers/menu' );
const express = require( 'express' );

const app = express();

app.use( express.json() );

app.use( '/menus', require( './routes/menu' ) );




app.listen( 3000, () => {
    console.log( `Servidor corriendo en puerto ${ 3000 }` );
} );



// ( async function () {
//     await createMenu( 'arroz chino', 10, 'almuerzo' );

//     console.log( 'registro ok' );
// } )();

// ( async function () {
//     const menus = await getMenus( );

//     console.log( menus );
// } )();
