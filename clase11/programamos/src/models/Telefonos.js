
const telefonos = [
    {
        marca: 'Samsung',
        modelo: 's11',
        gama: 'alta',
        precio: 2000
    },
    {
        marca: 'iphone',
        modelo: '12',
        gama: 'alta',
        precio: 2300
    },
    {
        marca: 'Xiaiomi',
        modelo: '121',
        gama: 'media',
        precio: 250
    },
    {
        marca: 'Huawei',
        modelo: 'y12',
        gama: 'media',
        precio: 2500
    },

    {
        marca: 'Huawei',
        modelo: 'y36',
        gama: 'baja',
        precio: 220
    },
];

module.exports = telefonos;