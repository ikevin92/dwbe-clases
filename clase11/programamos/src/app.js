const express = require( 'express' );
require( 'dotenv' ).config();
const app = express();

const telefonos = require( './models/Telefonos' );



app.get( '/telefonos', ( req, res ) => {



    res.json( telefonos );
} );

app.get( '/telefonos_ml', ( req, res ) => {

    // filtramos la mitad del array
    telefonos.splice( 0, ( telefonos.length / 2 ) );



    res.json( telefonos );
} );

app.get( '/telefonos/menor_precio', ( req, res ) => {


    let min = {};
    for ( let index = 0; index < telefonos.length; index++ ) {
        const element = telefonos[ index ];

        console.log( element.precio );

        if ( index > 0 && element.precio < telefonos[ index - 1 ].precio ) {
            min = element;
        }

    }

    res.json( min );
} );

app.get( '/telefonos/mayor_precio', ( req, res ) => {

    // filtramos la mitad del array
    const respuesta = telefonos.map( ( telefono, index ) => {
        telefono.precio;
    }
    );

    let max = {};
    for ( let index = 0; index < telefonos.length; index++ ) {
        const element = telefonos[ index ];

        console.log( element.precio );

        if ( index > 0 && element.precio > telefonos[ index - 1 ].precio ) {
            max = element;
        }

    }

    res.json( max );
} );

app.get( '/telefonos/agrupado', ( req, res ) => {
    const respuesta = {
        gama_alta: telefonos.filter( telefono => telefono.gama === 'alta' ),
        gama_media: telefonos.filter( telefono => telefono.gama ==='media' ),
        gama_baja: telefonos.filter( telefono => telefono.gama === 'baja' ),
    };

    res.json( respuesta );
} );



app.listen( 3000, () => console.log( `Servidor iniciado! ${ 3000 }` ) );
