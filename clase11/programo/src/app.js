const express = require( 'express' );
require( 'dotenv' ).config();
const app = express();


const lista_usuarios = ['user1', 'user2', 'user3']
const lista_productos = ['pollo', 'cebolla', 'platano']
// Get raiz de mi servicio web
// app.get( '/', ( req, res ) => { res.send( 'Hello World!' ); } );
app.use(express.static('./src/public'))
// CRUD DE USUARIOS
app.get( '/usuarios', ( req, res ) => { res.json( lista_usuarios ); } );
app.post( '/usuarios', ( req, res ) => { res.json( 'Post de usuarios' ); } );
app.put( '/usuarios', ( req, res ) => { res.json( 'put de usuarios' ); } );
app.delete( '/usuarios', ( req, res ) => { res.json( 'delete de usuarios' ); } );

// CRUD DE PRODUCTOS
app.get( '/productos', ( req, res ) => { res.json( lista_productos ); } );
app.post( '/productos', ( req, res ) => { res.json( 'Post de productos' ); } );
app.put( '/productos', ( req, res ) => { res.json( 'put de productos' ); } );
app.delete( '/productos', ( req, res ) => { res.json( 'delete de usuarios' ); } );


const PORT = process.env.PORT || 5000;

app.listen( PORT, () => console.log( `Servidor iniciado! ${ PORT }` ) );