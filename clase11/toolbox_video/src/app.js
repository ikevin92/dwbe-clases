const express = require( 'express' );
const app = express();
const port = 3000;

const paises = ['colombia', 'chile', 'panama']

app.get( '/', ( req, res ) =>
    res.send( 'Hello World!' )
);

app.get( '/paises', ( req, res ) => {
    
    for (const pais of paises) {
        console.log(pais)
    }

    return res.json( paises );
}
);

app.post( '/users', ( req, res ) =>

    // hago las validaciones

    res.send( 'el usuario se agrego correctamente' )


);

app.delete( '/users', ( req, res ) =>

    // hago las validaciones

    res.send( 'el usuario se elimino correctamente' )


);

app.listen( port, () => console.log( `escuchando desde el puerto ${port}` ) );