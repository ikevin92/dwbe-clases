class Usuario {
    constructor(nombre, apellido, email, pais, contrasena) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.pais = pais;
        this.contrasena = contrasena;
    }
}

let usuarios = [];
const registro = (usuario) => {
    // se valida que no exista el usuario
    const existe = usuarios.filter((user) => user.email === usuario.email);

    if (existe.length > 0) {
        return alert("El usuario ya exitse");
    } else {
        usuarios.push(usuario);
        return alert("Usuario creado");
    }
};


const login = (email, contrasena) => {
    console.log(email, contrasena);

    const res = usuarios.filter(
        (usuario) => usuario.email === email && usuario.contrasena === contrasena
    );
    console.log({ res });

    if (res.length > 0) {
        console.log("login exitoso");
        return alert("login exitoso");
    } else {
        console.log("error en las credenciales");
        return alert("error de credenciales");
    }
};



document.getElementById("button-submit").addEventListener("click", (e) => {
    e.preventDefault()
    const nombre = document.getElementById("nombre").value;
    const apellido = document.getElementById("apellido").value;
    const email = document.getElementById("email").value;
    const pais = document.getElementById("pais").value;
    const password = document.getElementById("password").value;
    const password2 = document.getElementById("password2").value;
    if (
        nombre !== "" &&
        apellido !== "" &&
        email !== "" &&
        pais !== "" &&
        password !== "" &&
        password2 !== ""

    ) {
        if (password === password2) {
            const usuario = new Usuario(nombre, apellido, email, pais, password);
            registro(usuario);
            console.log(usuarios);
        } else {
            alert("Las contraseñas no coinciden")
        }

    } else { // si algun campo está vacío
        alert("Algún campo está vacío")
    }
});

document.getElementById("login-submit").addEventListener('click', (e) => {
    e.preventDefault()
    const email_login = document.getElementById('email-login').value;
    const contrasena_login = document.getElementById('password-login').value;
    login(email_login, contrasena_login)
});
