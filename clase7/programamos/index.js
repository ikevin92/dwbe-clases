// OBJETO CLASE
class Persona {
    constructor ( nombre, apellido, edad ) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    fullName () {
        return `${ this.nombre } ${ this.apellido }`;
    }

    es_mayor () {
        return this.edad >= 18;
    }

    // getter y setter
    getNombre () {
        return this.nombre;
    }

    setNombre ( nombre ) {
        this.nombre = nombre;
    }
}

const kevin = new Persona( 'Kevin', 'Echeverri', 28 );
console.log( kevin.fullName(), kevin.es_mayor() );


// OBJETO FUNCION
function Persona2 ( nombre, apellido, edad ) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.edad = edad;

    this.fullName = function () {
        return `${ this.nombre } ${ this.apellido }`;
    };

    this.es_mayor = function () {
        return this.edad >= 18;
    };

}

const maria = new Persona2( 'Maria', 'Loboa', 57 );
console.log( kevin.fullName(), kevin.es_mayor() );