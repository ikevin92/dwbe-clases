

let anio_actual = 2021;

// creacion del objeto
let carro = {
    marca: 'Toyota',
    model: 'Fortuner',
    year: 2020,
    color: 'red',

    edad: function () {
        return anio_actual - this.year;
        // console.log( this.year );
    }
};

// agrega un atributo al objeto
carro.tipoCombustible = "Nafta";
carro.tituloVenta = function () {
    return `${ this.marca } - ${ this.model } a la venta`;
};

// imprime valor por consola
console.log( carro.tipoCombustible );
console.log( carro.edad() );
console.log( carro.tituloVenta );
console.log( carro );

// declarar objeto como una funcion
function Usuario ( nombreUsuario, nombre, apellido, email, contrasena ) {
    this.nombreUsuario = nombreUsuario;
    this.nombre = nombre;
    this.apellido = apellido;
    this.email = email;
    this.contrasena = contrasena;
}

// inicializamos 2 objetos
let u2 = new Usuario( 'koecheverri', 'Kevin', 'Echeverri', 'ikevin1992@gmail.com', '123456' );
let u3 = new Usuario( 'lmessi', 'Messi', 'Lionel', 'messi@gmail.com', '123456' );

console.log( u2.nombreUsuario );
console.log( u3.apellido );


// declaracion de objeto como clases
class Ropa {
    constructor ( marca, talla, color, precio, material ) {
        this.marca = marca;
        this.talla = talla;
        this.color = color;
        this.precio = precio;
        this.material = material;
    }

    obtenerTituloVenta () {
        return `Se vende ${ this.marca } con talla ${ this.talla } a un precio de ${ this.precio }`;
    }

}

let camiseta = new Ropa( 'Adidas', 'M', 'Azul', 30, 'Algodon' );

console.log( camiseta.obtenerTituloVenta() );

// desestructuracion
function mostrarRopa ( { marca, precio }  ) {
    
    console.log( precio );
    console.log( marca );
    
}

mostrarRopa( camiseta );


