const moment = require( 'moment' );


const now = moment().format( 'LLL' );
const ahora = moment( new Date() );
const utc = moment( new Date() ).utc();
const localidad = moment.locale( 'es' );


console.log( ahora.format( 'MMM Do YYYY, h:mm:ss a' ) );
console.log( ahora.utc().format( 'MMM Do YYYY, h:mm:ss a' ) );
console.log( 'Hola con nodemon' );

// diferencia utc
console.log( { ahora, utc, localidad, now } );
const diferencia = utc.diff( ahora );
const diferencia2 = ahora - utc;
console.log( { diferencia, diferencia2 } );

if ( ahora.isBefore( utc ) ) {
    console.log( 'ahora es menor a utc' );
} else {
    console.log( 'es diferente' );
}