// importamos los settings
const env = require( './appsettings.json' );
// llamado a la libreria npm i dotenv
const dotenv = require( 'dotenv' );

// instancia dotenv
dotenv.config();

console.log( 'desde dotenv', process.env.NODE_ENV );


console.log( env );

// si no tiene nadas definido en el dotenv me trae lo del env
const node_env = process.env.NODE_ENV || 'dev';
const variables = env[ node_env ];
console.log( node_env );
console.log( variables );