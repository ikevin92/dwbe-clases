
const resultado1 = 5 + 4;
const resultado2 = 5 + 1;
const resultado3 = resultado2 + 6;
const resultado4 = 5 / 4;


const obtenerResultado = ( operacion, valorA, valorB ) => {
    let resultado;

    switch ( operacion ) {
        case 'division':
            resultado = valorA / valorB;
            break;
        case 'suma':
            resultado = valorA + valorB;
            break;
        case 'multiplicacion':
            resultado = valorA * valorB;
            break;
        case 'resta':
            resultado = valorA - valorB;
            break;

        default:
            break;
    }

    return resultado;

};

console.log( obtenerResultado( 'division', 5, 6 ) );


