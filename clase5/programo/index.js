
// FOR

// for ( let indice = 0; indice < 10; indice++ ) {
//     console.log( indice );

// }


// let suma = 0;
// for ( let i = 0; i < 5; i++ ) {
//     let ingreso = parseInt( prompt( 'Ingrese un numero para sumar' ) );

//     if ( Number.isInteger( ingreso ) ) {
//         suma += ingreso;
//     }

// }

// alert( `la suma total es: ${ suma }` );


// WHILE
// let indice = 0;

let es_impar = false;

// while ( !es_impar ) {
//     let ingreso = parseInt( prompt( 'Ingrese un numero para sumar' ) );
//     if ( Number.isInteger( ingreso ) ) {
//         let resto = ingreso % 2;
//         console.log( resto );

//         if ( resto === 1 ) {
//             console.log( 'Es un numero impar' );
//             es_impar = true;
//         } else {
//             console.log( 'es un numero par' );
//         }
//     }

//     indice++;
// }


// arrays
let paises = [ 'Argentina', 'Bolivia', 'Colombia', 'Chile', 'Mexico', 'Uruguay' ];


// paises.unshift( 'Bolivia' );
// paises.shift( 'Venezuela' );

// paises.sort();

// paises.push( 7 );
// paises.push( true );
// paises.push( [ 'otro valor', 1, true, false, 4, 'test' ] );

// console.log( paises );
// console.log( paises[ 4 ] );

// paises.splice( 7, 1 );

// console.log( paises );

// for ( let index = 0; index < paises.length; index++ ) {
//     const pais = paises[ index ];
//     console.log( pais );
// }

// for ( const pais of paises ) {
//     console.log( pais );
// }

// paises.forEach( ( pais ) => {
//     console.log( pais );
// } );

// const paisesMayores5Letras = paises.map( ( pais ) => {

//     if ( pais.length > 5 ) {
//         return pais;
//     }
// } );


// console.log( paisesMayores5Letras );