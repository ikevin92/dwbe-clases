

let mi_variable = 'Kevin Echeverri 28';
let nombre = 'Kevin';
let apellido = 'Echeverri';
let edad = 38;

let mi_array = [ 'Kevin', 'Echeverri', 28 ];
console.log( typeof mi_array );

console.log( mi_array[ 1 ] );

// tipo de dato del objeto en donde esta el array
console.log( typeof mi_array[ 2 ] );

// añadir nuevo elemento al array
mi_array.push( 'ikevin1992@gmail.com' );

console.log( mi_array );

console.log( 'Cantidad de elementos del array:', mi_array.length );
console.log( mi_array[ 4 ] );


let paises = [ 'Argentina', 'Bolivia', 'Colombia', 'Chile', 'Mexico', 'Uruguay' ];


console.log( paises );

// ciclos con los array
for ( let index = 0; index < mi_array.length; index++ ) {
    const element = mi_array[ index ];

    console.log( { element } );
}

for ( let indice = 0; indice < paises.length; indice++ ) {
    const pais = paises[ indice ];

    console.log( { indice, pais } );
}


let indice = 0;

while ( indice < mi_array.length ) {
    //Imprime uno a uno todos los autos
    // Estamos haciendo lo mismo que con el for
    console.log( mi_array[ indice ] );
    indice++;
}






