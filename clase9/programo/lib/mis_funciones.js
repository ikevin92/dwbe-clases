function hablar ( algo ) {
    console.log( algo );
}

function hablar_bajito ( algo ) {
    console.log( algo.toLowerCase() );
}

// exportacion de las funciones 
exports.hablar = hablar;
exports.hablar_bajito = hablar_bajito;