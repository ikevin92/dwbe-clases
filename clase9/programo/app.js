console.log( 'hola mundo desde node' );


function holaMundo () {
    return 'hola mundo';
}

function msj ( mensaje ) {
    return mensaje;
}

console.log( msj( 'hola kevin' ) );

// array

const mascotas = [ 'loro', 'perro', 'gato', 'hamster' ];
//array de objetos
const mascotas_descripcion = [
    {
        nombre: 'Perro',
        descripcion: 'animal canino de compañia',
        estimado_vida: 12
    },
    {
        nombre: 'Gato',
        descripcion: 'animal felino de compañia',
        estimado_vida: 15
    },
    {
        nombre: 'Pajaro',
        descripcion: 'animal ave de compañia',
        estimado_vida: 5
    },
];

mascotas.push( 'tortuga' );

// for ( const mascota of mascotas ) {
//     console.log( mascota );
// }

mascotas.forEach( ( mascota, i ) => {
    console.log( mascota, i );
} );


// devuelve un array y sirve para un asincrono
const resultado = mascotas.map( ( mascota ) => {
    console.log( mascota );
    return "A-" + mascota;
} );

// retorna lo que se indique en la busqueda
const filtrado = mascotas.filter( ( mascota ) => mascota.length > 4 );

const index = mascotas.findIndex( mascota => mascota === 'perro' ); // sino encuentra envia -1 y si encuentra algo 1

// borrando elementos del array
mascotas.splice( 3, 1 ); // posicion, cantidad


console.log( { filtrado } );
console.log( { mascotas } );
console.log( { index } );

console.log( '-----------------------' );


const resultado_objeto = mascotas_descripcion.find( mascota => mascota.descripcion.length > 5 );

console.log( resultado_objeto );
