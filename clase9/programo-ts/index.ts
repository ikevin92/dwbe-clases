// let nombre = 'kevin';
// let altura = 1.79;
// let peso = 100;

// console.log( nombre );
// console.log( altura );
// console.log( peso );

let nombres = [ 'Kevin', 'Orlando', 'Maria' ];
nombres.push( 'Mario' );
console.log( nombres );

let numeros = [ 1, 2, 3, 5 ];
numeros.push( 7 );
console.log( numeros );

let combinado = [ 1, 'nombre', 2, 'apellido' ];
combinado.push( 1 );
combinado.push( 'letras' );
// combinado.push( true );

let mascota = {
    nombre: 'Perro',
    descripcion: 'Canino de compañia'
};

mascota.nombre = 'Gato';
mascota.descripcion = 'felino';

// mascota.edad = 12;  permite ingresar nuevas propiedades

let nombre_vacio: string;
nombre_vacio = 'onix';

let array: ( number | string )[] = [];
array.push( 'string' );
array.push( 4 );

let mascota_detallado: {
    nombre: string,
    raza: string,
    edad: number;

};