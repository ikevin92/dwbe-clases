let condicionUno = ( 1 == '1' );
let condicionDos = ( 1 === '1' );
let condicionTres = ( 0 || 1 );
let condicionCuatro = ( true && 1 );
let condicionCinco = ( 1 - 1 ) && ( 2 + 2 );
let condicionSeis = ( 1 - 1 ) || ( 2 + 2 );
let condicionSiete = ( 3 > '2' );


console.log( { condicionUno } );
console.log( { condicionDos } );
console.log( { condicionTres } );
console.log( { condicionCuatro } );
console.log( { condicionCinco } );
console.log( { condicionSeis } );
console.log( { condicionSiete } );



