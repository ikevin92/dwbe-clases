

console.log( 'condicionales' );


const resultado = 3 == 2;
console.log( resultado );


const numero1 = 5, numero2 = 7, numero3 = 6, numero4 = 12, nombre = 'Kevin';


if ( numero1 > numero2 ) {
    console.log( `El ${ numero1 }  es mayor a ${ numero2 }` );
} else {

    console.log( `El ${ numero1 } es menor a ${ numero2 }` );
}

// &&: AND
if ( ( numero1 > numero2 ) && ( numero1 > numero3 ) ) {
    console.log( 'se cumple' );
} else {
    console.log( 'no se cumple' );
}

// ||: OR
if ( ( numero1 > numero2 ) || ( numero1 > numero3 ) ) {
    console.log( 'se cumple alguna de las condiciones' );
} else {
    console.log( 'no se cumple ninguna de las dos' );
}

// switch
switch ( numero4 ) {
    case 1:
        console.log( 'es igual a uno' );
        break;
    case 2:
        console.log( 'es igual a dos' );
        break;
    case 500:
        console.log( 'es igual a 500' );
        break;

    default:
        console.log( 'no corresponde a ningun valor' );
        break;
}

switch ( nombre ) {
    case 'Kevin':
        console.log( `Felicidades ${ nombre }` );
        break;
    case 'Maria':
        console.log( `Felicidades ${ nombre }` );
        break;

    default:
        console.log( 'no corresponde a ningun valor' );
        break;
}

// ternario: ? :
const res = ( numero1 > numero2 ) ? 'es mayor' : 'es menor';
console.log( res );
