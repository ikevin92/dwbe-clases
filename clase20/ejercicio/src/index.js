const fetch = require( 'node-fetch' );

const key = "beb3801e";
const url = `https://omdbapi.com/?apikey=${ key }&t=`;

async function getMovie ( peli ) {
    const response = await fetch( url + peli );
    const result = await response.json();
    return result;
}

getMovie( "pacman" )
    .then( result => console.log( result ) );