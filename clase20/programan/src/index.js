
const express = require( 'express' );
const app = express();
const port = 3000;

const fetch = require( 'node-fetch' );

const key = "beb3801e";
const url = `https://omdbapi.com/?apikey=${ key }&t=`;

async function getMovie ( peli ) {
    const response = await fetch( url + peli );
    const result = await response.json();
    return result;
}

app.post( '/movies', async ( req, res ) => {

    const { title } = req.body;
    const movie = await getMovieInfo( title );

    return res.status( 200 ).send( movie );

} );

app.post( '/', function ( req, res ) {
    console.log(req.body)
    res.send( 'POST request to homepage' )
    
} )



app.get( '/movies', async ( req, res ) => {

    console.log( req.query );

    const { name } = req.query;

    const data = await getMovie( name );

    // console.log( data );

    // res.send( 'servicio get' );
    res.json( data );

} );

app.get( '/movies/:title', async ( req, res ) => {

    // console.log( req.query );

    const { title } = req.params;

    const data = await getMovie( title );

    // console.log( data );

    // res.send( 'servicio get' );
    res.json( data );

} );




app.listen( port, () => console.log( `Example app listening on port port!` ) );